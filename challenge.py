# Patrick Renschler <patrick.renschler@gmail.com>
# Oct 12, 2015

"""
20n Coding Challenge: Classify Wikipedia Disease Articles
"""

import os
import re
import random
import nltk
from collections import defaultdict
from nltk import word_tokenize, sent_tokenize
from bs4 import BeautifulSoup

def articleFeatures(filename):
	""" Given a filename of a wikipedia article, return a dictionary of features for the disease classification problem
	Features:
	- words from first sentence of lead section of wiki article
	- presence of Classification and External Resources Table
	"""
	features = {}

	relevant_words = ["syndrome", "characterized", "disease", "condition","disorders",
	 "primary", "congenital", "abnormal", "specific", "involuntary", "disorder",
	 "abnormality", "viruses", "anxiety", "infection", "bacteria", "partial", "accumulation",
	 "weakness", "skeletal", "aversion", "presence", "coagulation", "certain",
	 "symptoms", "organism", "proximal", "medication", "inflammation", "herpesvirus", "virus"
	 "panophobia", "abnormally", "symptomatic", "asymptomatic", "syndromes", "inflammatory",
	 "rare"]
	
	# parse html with beautifulSoup
	f = open(filename)
	raw_HTML = f.read().decode('utf8')
	html = BeautifulSoup(raw_HTML, "html.parser")
	
	# get words from lead sentence and insert into features dict
	paragraphs = html.select_one("p")
	if paragraphs:
		leadSection = paragraphs.get_text()
		if not leadSection:
			i = 1
			paragraphs = html.select("p")
			while not leadSection and i < len(paragraphs):
				leadSection = paragraphs[i].get_text()
				i = i + 1
		if leadSection:
			firstSentence = sent_tokenize(leadSection)[0]
			tokens = word_tokenize(firstSentence)
			tokens = [t.lower().encode('utf-8') for t in tokens if len(t) > 6]
			tokenSet = set(tokens)
			for word in relevant_words:
				features['contains({})'.format(word)] = (word in tokenSet)
	
	# Classification and external resources infobox
	table_infobox = html.select_one("table.infobox")
	if table_infobox:
		infoboxText = table_infobox.get_text()
		features['contains(Classification_and_external_resources)'] = bool(re.search('Classification and external resources', infoboxText))
	else:
		features['contains(Classification_and_external_resources)'] = False
	return features

def buildFeatureSets():
	""" returns a list of feature sets built from the bucketed training data
	"""
	pos = [(articleFeatures("training/positive/"+filename), 'positive') for filename in os.listdir('training/positive')]
	neg = [(articleFeatures("training/negative/"+filename), 'negative') for filename in os.listdir('training/negative')]
	featuresets = pos + neg
	random.shuffle(featuresets)
	return featuresets

def buildTestFeatureSets():
	""" returns a list of feature sets built from the bucketed test data
	"""
	pos = [(articleFeatures("test/positive/"+filename), 'positive') for filename in os.listdir('test/positive')]
	neg = [(articleFeatures("test/negative/"+filename), 'negative') for filename in os.listdir('test/negative')]
	featuresets = pos + neg
	random.shuffle(featuresets)
	return featuresets

def getDiseaseInfo(filename):
	""" Given a filename of a wikipedia article on disease, return a dictionary of relevant information about the disease
	"""
	diseaseInfo = {}
	f = open(filename)
	raw_HTML = f.read().decode('utf8')
	html = BeautifulSoup(raw_HTML, "html.parser")
	## disease name
	heading = html.select("#firstHeading")
	if heading:
		diseaseInfo['name'] = heading[0].get_text()
	
	### Other disease info not supported

	## symptoms
	# scarpe wiki for symptoms section
	# scrape mayo clinic disease page for symptoms info
	
	## treatment
	# scrape wikipage for treatment section
	# scrape mayo clinic disease page for treatment info

	return diseaseInfo

def main():
	######## Part 1: Create a classifier that can accurately predict whether an article describes a disease
	print "Part 1 ##################"
	print "Warning it took 15 minutes to build the feature set on my laptop"
	featuresets = buildFeatureSets()
	train_set, test_set = featuresets[:10000], featuresets[10000:]
	classifier = nltk.DecisionTreeClassifier.train(train_set)
	print "Classifier Accuracy on Test set: " + str(nltk.classify.accuracy(classifier, test_set))

	## for testing against your own test set
	# testFeatureSets = buildTestFeatureSets() # you may have to modify this function to match your filename and folder hierarchy
	# print nltk.classify.accuracy(classifier, testFeatureSets)

	######## Part 2: Extract the name of the disease
	print "Part 2 ##################"
	print "Use the getDiseaseInfo(filename) function to retrieve a dictionary of information about that disease"
	print "Ex: getDiseaesInfo('training/positive/Acatalasemia').get('name') = " + getDiseaseInfo('training/positive/Acatalasemia').get('name')


if __name__ == "__main__":
	#import doctest
	#doctest.testmod()
	main()