Instructions for running:

(1) Clone the repo: https://RnR66HNh@bitbucket.org/RnR66HNh/20n.git

(2) Unzip the training.zip file

(3) Using python 2.7, run the command: pip install -r requirements.txt
to install the nltk and beautifulsoup4 libraries.

(4) Execute demo with the command: python challenge.py
(This takes ~15 min to execute on my machine)

(5) To test against your own test set,  you can modify the main function in challenge.py, you might want to do this before you run the demo because otherwise you will have to wait another 15 minutes to regenerate the classifier.

Notes:

- I worked on this for a total of 5 hours today.

- My first classifier relied only on the presence of the "Classification and external resources" table. The accuracy of this classifier against my test set was around 95%, with a 14% false negative rate and 0.001% false positive rate. I decided to add some more basic features by tokenizing the first sentence of the lead section.

- The first time I built the decision tree classifier with the lead section words, I included all encountered words in the feature set. I pruned this decision tree to a depth of 35 and saved a list of the decision node words. These words are now used to build the feature sets - to limit the number of features the classifier-builder needs to process on your machine. In other words, instead of considering all words for the feature set, I only evaluate the presence of words in the relevant_words list.

- Going forwards I would be interested in trying to account for the context of words in the lead section during feature extraction, maybe by extracting bigrams.

- I noticed that some of the files in the training set are mislabeled. For example, "Fructose-1,6-bisphosphatase_deficiency" is labeled as positive, but the HTML dump actually refers to the "Fructose_1,6-bisphosphatase" wikipedia entry. I saw several files in the positive folder that had a similar issue, sorry I didn't write down all the names. In the negative folder I found 8 files that I think are misclassified. They are listed in MaybeMisclassified.txt.

- Edge cases: My classifier seems to do fine with the first edge case but current fails for the second edge case (generic forms of diseases). Since I used a decision tree, I could mess with things by hand and add decision nodes with phrases that reflect this 'generic nature' like "group of diseases". I could also check to see if the first <b> tags in the lead section are preceeded by the articles 'a' or 'an'. This might suggest that the article is about a generic class of diseases.